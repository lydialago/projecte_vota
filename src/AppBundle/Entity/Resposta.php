<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Resposta
 */

/**
 * Entitat Resposta de la base de dades
 * @ORM\Table(name="resposta")
 * @ORM\Entity
 */
class Resposta
{
    /**
     * Identificador de la resposta
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * Valor de la resposta. True si la resposta es un 'si', false si la resposta es un 'no'
     * @var boolean
     *
     * @ORM\Column(name="valor", type="boolean", nullable=false)
     */
    private $valor;

    /**
     * Data de la resposta
     * @var \DateTime
     *
     * @ORM\Column(name="data", type="datetime", nullable=false)
     */
    private $data = 'CURRENT_TIMESTAMP';

    /**
     * Identificador de l'enquusuariesta
     *
     * @ORM\ManyToOne(targetEntity="Usuari", inversedBy="respostes")
     * @ORM\JoinColumn(name="usuari_id", referencedColumnName="id")
     */
    private $usuari;

    /**
     * Identificador de l'enquesta
     *
     * @ORM\ManyToOne(targetEntity="Enquesta", inversedBy="respostes")
     * @ORM\JoinColumn(name="enquesta_id", referencedColumnName="id")
     */
    private $enquesta;


    /**
     * Get valor
     *
     * @return bool
     */
    public function getValor()
    {
        return $this->valor;
    }

    /**
     * Set valor
     *
     * @param $valor
     * @return $this
     */
    public function setValor($valor)
    {
        $this->valor = $valor;

        return $this;
    }

    /**
     * Get data
     *
     * @return \DateTime
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * Set data
     *
     * @param $data
     * @return $this
     */
    public function setData($data)
    {
        $this->data = $data;

        return $this;
    }

    /**
     * Get usuari
     *
     * @return int
     */
    public function getUsuari()
    {
        return $this->usuari;
    }

    /**
     * Set usuari
     *
     * @param $usuari
     * @return $this
     */
    public function setUsuari($usuari)
    {
        $this->usuari = $usuari;

        return $this;
    }

    /**
     * Get enquesta
     *
     * @return int
     */
    public function getEnquesta()
    {
        return $this->enquesta;
    }

    /**
     * Set enquesta
     *
     * @param $enquesta
     * @return $this
     */
    public function setEnquesta($enquesta)
    {
        $this->enquesta = $enquesta;

        return $this;
    }

    /**
     * Get identificador de la resposta
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set identificador de la resposta
     *
     * @return  self
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }
}
