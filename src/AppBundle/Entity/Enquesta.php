<?php

// src/AppBundle/Entity/Enquesta.php

namespace AppBundle\Entity;
use Doctrine\ORM\Mapping as ORM;

/**
 * Enquesta
 */

/**
 * Entitat Enquesta de la base de dades
 * @ORM\Table(name="enquesta")
 * @ORM\Entity
 */
class Enquesta
{
    /**
     * Pregunta de l'enquesta
     * @var string
     *
     * @ORM\Column(name="pregunta", type="string", length=250, nullable=false)
     */
    private $pregunta;

    /**
     * Data inici de l'enquesta per votar
     * @var \DateTime
     * @ORM\Column(name="data_inici", type="date", nullable=false)
     */
    private $dataInici;

    /**
     * Data final de l'enquesta per votar
     * @var \DateTime
     *
     * @ORM\Column(name="data_final", type="date", nullable=false)
     */
    private $dataFinal;

    /**
     * Enquesta destacada o no
     * @var boolean
     *
     * @ORM\Column(name="destacada", type="boolean", nullable=false)
     */
    private $destacada;

    /**
     * Identificador de l'enquesta
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @ORM\OneToMany(targetEntity="Resposta", mappedBy="enquesta")
     */
    private $respostes;

    public function __construct()
    {
        $this->respostes = new ArrayCollection();
    }

    /**
     * Set pregunta
     *
     * @param string $pregunta
     *
     * @return Enquesta
     */
    public function setPregunta($pregunta)
    {
        $this->pregunta = $pregunta;

        return $this;
    }

    /**
     * Get pregunta
     *
     * @return string
     */
    public function getPregunta()
    {
        return $this->pregunta;
    }

    /**
     * Set dataInici
     *
     * @param \DateTime $dataInici
     *
     * @return Enquesta
     */
    public function setDataInici($dataInici)
    {
        $this->dataInici = $dataInici;

        return $this;
    }

    /**
     * Get dataInici
     *
     * @return \DateTime
     */
    public function getDataInici()
    {
        return $this->dataInici;
    }

    /**
     * Set dataFinal
     *
     * @param \DateTime $dataFinal
     *
     * @return Enquesta
     */
    public function setDataFinal($dataFinal)
    {
        $this->dataFinal = $dataFinal;

        return $this;
    }

    /**
     * Get dataFinal
     *
     * @return \DateTime
     */
    public function getDataFinal()
    {
        return $this->dataFinal;
    }

    /**
     * Set destacada
     *
     * @param boolean $destacada
     *
     * @return Enquesta
     */
    public function setDestacada($destacada)
    {
        $this->destacada = $destacada;

        return $this;
    }

    /**
     * Get destacada
     *
     * @return boolean
     */
    public function getDestacada()
    {
        return $this->destacada;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get the value of products
     */
    public function getProducts()
    {
        return $this->products;
    }

    /**
     * Set the value of products
     *
     * @return  self
     */
    public function setProducts($products)
    {
        $this->products = $products;

        return $this;
    }

    /*Get answers */
    public function getNumRespostes()
    {
        $numSi = 0;
        $numNo = 0;
        $numRes = 0;
        foreach($this->getRespostes() as $resposta){
            $numRes++;
            if($resposta->getValor()) $numSi++;
            else $numNo++;
        }
        return array(
            'numRes' => $numRes,
            'numSi' => $numSi,
            'numNo' => $numNo,
        );
    }

    /**
     * Get the value of respostes
     */
    public function getRespostes()
    {
        return $this->respostes;
    }

    /**
     * Set the value of respostes
     *
     * @return  self
     */
    public function setRespostes($respostes)
    {
        $this->respostes = $respostes;

        return $this;
    }
}
