<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * EnquestaType servei.
 */

/**
 * Servei per crear els formularis de crear i editar enquesta
 */

class EnquestaType extends AbstractType
{

    /**
     * Crea el formulari amb els camps especificats
     * 
     * {@inheritdoc}
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('pregunta')->add('dataInici')->add('dataFinal')->add('destacada');
    }


    /**
     * {@inheritdoc}
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Enquesta'
        ));
    }


    /**
     * {@inheritdoc}
     * @return string
     */
    public function getBlockPrefix()
    {
        return 'appbundle_enquesta';
    }


}
