<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Enquesta;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use \Datetime;

/**
 * Enquestes admin controller.
 */

/**
 * Controlador de l'admin per indexar, crear, eliminar i editar enquestes
 */

/**
 * @Route("admin")
 */
class EnquestaController extends Controller
{

    /**
     * Lists all enquestum entities.
     *
     * @Route("/", name="enquesta_index")
     * @Method("GET")
     * @return mixed
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $enquestas = $em->getRepository('AppBundle:Enquesta')->findAll();

        foreach ($enquestas as $enquesta){
            $respostes = $enquesta->getNumRespostes();

            $enquesta->totalRespostes = $respostes['numRes'];
            $enquesta->totalRespostesSi = $respostes['numSi'];
            $enquesta->totalRespostesNo = $respostes['numNo'];

            $enquesta->activa = false;
            $dataInici = $enquesta->getDataInici();
            $dataFinal = $enquesta->getDataFinal();
            $dataActual = new Datetime(date("y-m-d H:i:s"));

            if($dataActual > $dataInici && $dataActual < $dataFinal){
                $enquesta->activa = true;
            }
        }
        return $this->render('enquesta/index.html.twig', array(
            'enquestas' => $enquestas,
        ));
    }

    /**
     * Creates a new enquestum entity.
     *
     * @Route("/new", name="enquesta_new")
     * @Method({"GET", "POST"})
     * @param Request $request
     * @return mixed
     */
    public function newAction(Request $request)
    {
        $enquestum = new Enquesta();
        $form = $this->createForm('AppBundle\Form\EnquestaType', $enquestum);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($enquestum);
            $em->flush();

            return $this->redirectToRoute('enquesta_show', array('id' => $enquestum->getId()));
        }

        return $this->render('enquesta/new.html.twig', array(
            'enquestum' => $enquestum,
            'form' => $form->createView(),
            'resposta' => 'Enquesta desada correctament.',
        ));
    }

    /**
     * Finds and displays a enquestum entity.
     *
     * @Route("/{id}", name="enquesta_show")
     * @Method("GET")
     * @param Enquesta $enquestum
     * @return mixed
     */
    public function showAction(Enquesta $enquestum)
    {
        $deleteForm = $this->createDeleteForm($enquestum);

        return $this->render('enquesta/show.html.twig', array(
            'enquestum' => $enquestum,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing enquestum entity.
     *
     * @Route("/{id}/edit", name="enquesta_edit")
     * @Method({"GET", "POST"})
     * @param Request $request
     * @param Enquesta $enquestum
     * @return mixed
     */
    public function editAction(Request $request, Enquesta $enquestum)
    {
        $pregunta = $enquestum->getPregunta();
        $deleteForm = $this->createDeleteForm($enquestum);
        $editForm = $this->createForm('AppBundle\Form\EnquestaType', $enquestum);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $enquestum->setPregunta($pregunta);
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('enquesta_edit', array('id' => $enquestum->getId()));
        }

        return $this->render('enquesta/edit.html.twig', array(
            'enquestum' => $enquestum,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView()
        ));
    }

    /**
     * Deletes a enquestum entity.
     *
     * @Route("/{id}", name="enquesta_delete")
     * @Method("DELETE")
     * @param Request $request
     * @param Enquesta $enquestum
     * @return mixed
     */
    public function deleteAction(Request $request, Enquesta $enquestum)
    {
        $form = $this->createDeleteForm($enquestum);
        $form->handleRequest($request);
        $idEnquesta = $enquestum->getId();
        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($enquestum);
            $totalRespostes = $em->getRepository('AppBundle:Resposta')->findBy(array('idEnquesta' => $idEnquesta));
            foreach ($totalRespostes as $resposta){
                $em->remove($resposta);
            }
            $em->flush();
        }

        return $this->redirectToRoute('enquesta_index');
    }

    /**
     * Creates a form to delete a enquestum entity.
     *
     * @param Enquesta $enquestum The enquestum entity
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Enquesta $enquestum)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('enquesta_delete', array('id' => $enquestum->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
