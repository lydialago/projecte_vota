<?php
// src/AppBundle/Controller/RegistrationController.php
namespace AppBundle\Controller;

use AppBundle\Form\UsuariType;
use AppBundle\Entity\Usuari;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

/**
 * Registration controller.
 */

 /**
 * Controlador encarregat del formulari de registre registre d'usuaris a la bddd
 */
class RegistrationController extends Controller
{

    /**
     * Registre d'un usuari
     *
     * @Route("/register", name="user_registration")
     * @param Request $request
     * @param UserPasswordEncoderInterface $passwordEncoder
     * @return mixed
     */
    public function registerAction(Request $request, UserPasswordEncoderInterface $passwordEncoder)
    {
        // 1) Assignar el formulari a un Usuari
        $usuari = new Usuari();
        $form = $this->createForm(UsuariType::class, $usuari);

        // 2) Comprovar que el mètode sigui POST i els camps vàlids
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {

            // 3) Codificar la password i guardar-la
            $password = $passwordEncoder->encodePassword($usuari, $usuari->getPlainPassword());
            $usuari->setPassword($password);

            // 4) Gardar l'Usuari a la BBDD
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($usuari);
            $entityManager->flush();

            return $this->redirectToRoute('user_registration');
        }

        return $this->render(
            'registration/register.html.twig',
            array('form' => $form->createView())
        );
    }
}
