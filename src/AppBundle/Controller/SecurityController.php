<?php
// src/AppBundle/Controller/SecurityController.php
namespace AppBundle\Controller;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Security controller.
 */
class SecurityController extends Controller
{
    /**
     * Logueja un usuari i determina si es normal o admin
     *
     * @Route("/login", name="login")
     * @param Request $request
     * @return JsonResponse
     */
    public function loginAction(Request $request)
    {
        //Si no és admin retorna l'html de l'estructura de la llista
        if (!$this->get('security.authorization_checker')->isGranted('ROLE_ADMIN')) {
            $response = array(
                "code" => 200,
                "response" => $this->render('public/enquestes.html.twig')->getContent());
            return new JsonResponse($response);
        }
        //Si és admin torna un petit Json indicant que és admin (serà redirigit després amb javascript)
        return new JsonResponse(array('data' => 'ADMIN'));
    }

    /**
     * Logout
     *
     * @Route("/logout", name="logout")
     */
    public function logoutAction()
    {
        return redirectToRoute('homepage');
    }
}
