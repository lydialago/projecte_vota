<?php
namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use AppBundle\Entity\Enquesta;
use AppBundle\Entity\Resposta;
use Symfony\Component\HttpFoundation\ParameterBag;
use \Datetime;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * Ajax controller.
 */

 /**
  * Controlador encarregat de les respostes Ajax a la pantalla de l'usuari alumne
  */
class AjaxController extends Controller
{

    /**
     * Extreure totes les enquestes amb les respostes de la bbdd
     *
     * @Route("/enquestes/totes", name="llista_enquestes")
     * @param Request $request
     * @param UserInterface $user
     * @return JsonResponse
     */
    public function llistaEnquestes(Request $request, UserInterface $user)
    {
        //Treure totes les enquestes de la BBDD
        $em = $this->getDoctrine()->getManager();
        $enquestas = $em->getRepository('AppBundle:Enquesta')->findAll();
        $resposta = "";
        foreach ($enquestas as $enquesta){      //Per cada enquesta
            //Aconseguir l'id de l'enquesta i contar les respostes assignades a la mateixa
            $respostes = $enquesta->getNumRespostes();

            //Comprovar si l'enquesta està activa o no
            $enquestaActiva = false;
            $dataInici = $enquesta->getDataInici();
            $dataFinal = $enquesta->getDataFinal();
            $dataActual = new Datetime(date("y-m-d H:i:s"));
            //Clacular la diferència entre la data final i l'actual
            $diff = date_diff($dataActual,$dataFinal);
            //Comprovar les dates de l'enquesta per saber si està activa o no
            if($dataActual > $dataInici && $dataActual < $dataFinal)
                $enquestaActiva = true;

            //Comprovar si l'enquesta està resposta o no (sempre hauria de sortir que esta contestada)
            $enquestaResposta = false;
            $respostaPerEnquesta = $em->getRepository('AppBundle:Resposta')->findBy(array('enquesta' => $enquesta, 'usuari' => $user));
            if(!empty($respostaPerEnquesta)){
                $enquestaResposta = true;
            }

            //Preparar un array de dades que utilitzarem per renderitzar la plantilla twig
            $temp = array(
                'id' => $enquesta->getId(),
                'pregunta' => $enquesta->getPregunta(),
                'dataInici' => $dataInici->format('d-m-Y'),
                'dataFi' => $dataFinal->format('d-m-Y'),
                'diferenciaDates' => $diff->format("%a"),
                'activa' => $enquestaActiva,
                'nRespostes' => $respostes['numRes'],
                'nRespostesSi' => $respostes['numSi'],
                'nRespostesNo' => $respostes['numNo'],
                'estaResposta' => $enquestaResposta,
                'senseContenidor'=> false       //Fa que la plantilla porti contenidor a l'enquesta
             );
             //Afegir la plantilla twig renderitzada de l'enquesta a $resposta que contindrà tot l'html seguit
             $resposta = $resposta . $this->render('public/enquesta.html.twig', array('enquesta' => $temp))->getContent();
        }
        //Enviar la l'html amd totes les enquestes com a resposta a la crida ajax
        $response = array(
            "code" => 200,
            "response" => $resposta);
        return new JsonResponse($response);
    }


    /**
     * Crea una respota a partir del camps de la request i fa l'insert a la bbdd
     *
     * @Route("/respon/{id}", name="respon_enquesta")
     * @param Request $request
     * @param int $id
     * @param UserInterface $user
     * @return JsonResponse
     */
    public function responEnquesta(Request $request, int $id, UserInterface $user)
    {
        $em = $this->getDoctrine()->getManager();
        //Crea un objecte resposta nou buit
        $resposta = new Resposta();
        //Asigna el valor segons el rebut per ajax
        if($request->request->get('answer') == "true")
            $resposta->setValor(true);
        else
            $resposta->setValor(false);
        //Assigna els id's de l'usuari i de l'enquesta i posa la data actual com a data de resposta
        $resposta->setUsuari($user);
        $resposta->setEnquesta($em->getRepository('AppBundle:Enquesta')->find($id));
        $resposta->setData(new \DateTime("now"));

        //Persistir l'objecte resposta a la BBDD
        $em->persist($resposta);
        $em->flush();

        //Buscar l'enquesta i contar les respostes assignades a la mateixa
        $enquesta = $em->getRepository('AppBundle:Enquesta')->find($id);
        $respostes = $enquesta->getNumRespostes();

        //Comprovar si l'enquesta està activa o no
        $enquestaActiva = false;
        $dataInici = $enquesta->getDataInici();
        $dataFinal = $enquesta->getDataFinal();
        $dataActual = new Datetime(date("y-m-d H:i:s"));
        $diff = date_diff($dataActual,$dataFinal);

        if($dataActual > $dataInici && $dataActual < $dataFinal){
            $enquestaActiva = true;
        }

        //Comprovar si l'enquesta està resposta o no (sempre hauria de sortir que esta contestada)
        $enquestaResposta = false;
        $respostaPerEnquesta = $em->getRepository('AppBundle:Resposta')->findBy(array('enquesta' => $enquesta, 'usuari' => $user));
        if(!empty($respostaPerEnquesta)){
            $enquestaResposta = true;
        }
        //Preparar un array de dades que utilitzarem per renderitzar la plantilla twig
        $temp = array(
            'id' => $id,
            'pregunta' => $enquesta->getPregunta(),
            'dataInici' => $dataInici->format('d-m-Y'),
            'dataFi' => $dataFinal->format('d-m-Y'),
            'diferenciaDates' => $diff->format("%a"),
            'activa' => $enquestaActiva,
            'nRespostes' => $respostes['numRes'],
            'nRespostesSi' => $respostes['numSi'],
            'nRespostesNo' => $respostes['numNo'],
            'estaResposta' => $enquestaResposta,
            'senseContenidor'=> true    //Fa que la plantilla no porti contenidor a l'enquesta
        );

        //Renderitzar la plantilla twig i enviar el resultat com a resposta ajax
        $resposta = $this->render('public/enquesta.html.twig', array('enquesta' => $temp))->getContent();
        $response = array(
            "code" => 200,
            "response" => $resposta);
        return new JsonResponse($response);
    }
}
