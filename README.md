# Projecte Vota!
Integrants del projecte:

- Lydia Lago
- Arantxa Cardona
- Joan Vilarrasa

## Url del projecte
- [Vota!](http://votaproject.tk/)

## Objectiu del projecte
L'objectiu es crear una pàgina web on es poden crear enquestes a partir del administrador i poder contestar-les a partir dels usuaris. Haurà usuaris i es podràn veure els diversos resultats d'aquests.

## Adreça de les nostres hores fetes

 - [Full de seguiment](https://docs.google.com/spreadsheets/d/1HmrDYnVEshxptsVsT9YbO0nnYHFrvlRSTDZc3nEjqas/edit?usp=sharing)
 - [Especificacions](https://docs.google.com/document/d/18FNgE8sKABWWu34ao8X2UjuOjtrsYVlESJIyNlH6mfo/edit?usp=sharing)

## Fulls prototip de la pàgina

- [Part usuari + login](https://wireframepro.mockflow.com/view/D6605f048ac45ef4eca81b0699f0bc684#/page/6e6f26f83e1e476a83a450dd26c346bd)
- [Part admin](https://wireframepro.mockflow.com/view/Dacb043b7bc8a06815c541a2970d95bc5#/page/fc16cc73fbbc46ee8a99194bc2b4e070)

## PHPDocumentation
Tota la documentació es troba al directori /doc.

## GIT
BRANCA PROPIA  
$ git add .  
$ git commit -m "xxxxx"  

BRANCA MASTER (git checkout master)  
git pull  
Actualitza la branca master amb tots el canvis que els altres han fet  

BRANCA PROPIA  
$ git rebase master  
Col·loca el principi de la nostra branca al final de la master actualitzada. A continuació hi ha dues opcions:  
1.  
$ git push origin [branca propia]  
Es fa una pull request a bitbucket per a que altes revisin el teu codi i desde bitbucket es fa el merge  
2.  
$ git push origin master  
Es fa un push directament del teu repositori a la master  

	- git remote rm origin
	- git remote add origin https://XXXXXXX@bitbucket.org/lydialago/projecte_vota.git
	- git config --global user.email "you@example.com"
	- git config --global user.name "Your Name"
	- git branch --set-upstream-to=origin/master master