<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * Default controller.
 */

  /**
  * Controlador de l'arrel del projecte. Si no hi ha cap usuari loguejat renderitza la plantilla de login i torna les enquestes destacades. 
  */
class DefaultController extends Controller
{

    /**
     * @Route("/", name="homepage")
     * @param Request $request
     * @return mixed
     */
    public function indexAction(Request $request)
    {
        //Si està loguejat com a admin el redirigeix a la part de administració
        if ($this->get('security.authorization_checker')->isGranted('ROLE_ADMIN')) {
            return $this->redirectToRoute('enquesta_index');
        }
        //Si està loguejat però no és admin apareixerà directament la llista per tant no cal buscar enquestes destacades
        else if ($this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY')) {
            return $this->render('public/index.html.twig');
        }
        //Trobar totes les enquestes destacades
        $em = $this->getDoctrine()->getManager();
        $enquestes_destacades = $em->getRepository('AppBundle:Enquesta')->findByDestacada(true);
        $destacades = array();
        //Agafar les 4 primeres enquestes destacades
        foreach (array_slice($enquestes_destacades, 0, 4) as $enquesta){
            //Aconseguir l'id de l'enquesta i contar les respostes assignades a la mateixa
            $idEnquesta = $enquesta->getId();
            $totalRespostes = count($em->getRepository('AppBundle:Resposta')->findBy(array('idEnquesta' => $idEnquesta)));
            $totalRespostesSi = count($em->getRepository('AppBundle:Resposta')->findBy(array('idEnquesta' => $idEnquesta, 'valor' => 1)));
            $totalRespostesNo = count($em->getRepository('AppBundle:Resposta')->findBy(array('idEnquesta' => $idEnquesta, 'valor' => 0)));
            //Preparar un array de dades que utilitzarem per renderitzar la plantilla twig
            $temp = array(
                'pregunta' => $enquesta->getPregunta(),
                'nRespostes' => $totalRespostes,
                'nRespostesSi' => $totalRespostesSi,
                'nRespostesNo' => $totalRespostesNo
            );
            //Afegir l'array a l'array de enquestes destacades
            $destacades[$idEnquesta] = $temp;
        }
        //Renderitzar la plantilla envian-li les enquestes destacades
        return $this->render('public/index.html.twig', array('destacades' => $destacades));
    }
}

