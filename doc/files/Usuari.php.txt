<?php
// src/AppBundle/Entity/Usuari.php
namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * Usuari
 */

/**
 * Entitat Usuari de la base de dades
 * @ORM\Table(name="usuari")
 * @ORM\Entity
 * @UniqueEntity(fields="username", message="Username already taken")
 */
class Usuari implements UserInterface, \Serializable
{
    /**
     * @var integer
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(type="string", length=25, unique=true)
     */
    private $username;

    /**
     * @Assert\NotBlank()
     * @Assert\Length(max=4096)
     */
    private $plainPassword;

    /**
     * @var string
     * @ORM\Column(type="string", length=64)
     */
    private $password;

    /**
     * @var boolean
     * @ORM\Column(name="is_active", type="boolean")
     */
    private $isActive;

    /**
     * @var array 
     * @ORM\Column(type="json_array")
     */
    private $roles = array();

    /**
     * Get plainPassword
     * 
     * @return mixed
     */
    public function getPlainPassword()
    {
        return $this->plainPassword;
    }

    /**
     * Set plainPassword
     * 
     * @param $password
     */
    public function setPlainPassword($password)
    {
        $this->plainPassword = $password;
    }

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->isActive = true;
    }

    /**
     * Get userName
     * 
     * @return mixed
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * Set userName
     * 
     * @param $username
     */
    public function setUsername($username)
    {
        $this->username = $username;
    }

    /**
     * Get salt
     * 
     * @return null
     */
    public function getSalt()
    {
        return null;
    }

    /**
     * Get password
     * 
     * @return mixed
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * Set password
     * 
     * @param $password
     */
    public function setPassword($password)
    {
        $this->password = $password;
    }

    /**
     * Get roles
     * 
     * @return array
     */
    public function getRoles()
    {
        $roles = $this->roles;
        $roles[] = 'ROLE_USER';

        return array_unique($roles);
    }

    /**
     * Set roles
     * 
     * @param array $roles
     * @return $this
     */
    public function setRoles(array $roles)
    {
        $this->roles = $roles;

        // allows for chaining
        return $this;
    }

    /**
     *
     */
    public function eraseCredentials()
    {
    }

    /** 
     * @see \Serializable::serialize()
     */
    public function serialize()
    {
        return serialize(array(
            $this->id,
            $this->username,
            $this->password,
        ));
    }

    /** 
     * @see \Serializable::unserialize() 
     */
    public function unserialize($serialized)
    {
        list (
            $this->id,
            $this->username,
            $this->password,
        ) = unserialize($serialized);
    }

    /**
     * Get id
     * 
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set id
     * 
     * @param $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }
}

