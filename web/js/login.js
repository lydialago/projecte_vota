/*---------------------------------------------------------------------------------------*/
/*---------------------------------------[ LOGIN ]---------------------------------------*/

/*Quan es sotmet el formulari de login valida els caps i fa la petició ajax*/
function loginListener(){
    $('#login').click(function() {
        //Comprovar que els camps no estiguin buits
        if(validaLogin()){
            /*Agafar les dades dels camps*/
            let username = $('#username_input').val();
            let password = $('#password_input').val();
            //Convertir les dades de login a un objecte JSON
            let data = JSON.stringify({username: username, password: password});
            //Executar la request de login amb ajax
            let baseurl = window.location.origin+window.location.pathname;
            $.ajax({
                url: baseurl+'login',
                type: 'POST',
                contentType: "application/json",
                dataType: 'json',
                data: data,
                success: function (data, status) {
                    //En cas que l'usuari sigui admin redirigir a la pantalla de admin
                    if(data.data=="ADMIN") window.location.href = '/admin';
                    //Col·locar el botó per poder fer el logout a la part superior dreta de la pantalla
                    $('#logout').html('<li class="nav-item"><a class="nav-link" href="/logout">Logout</a></li>');
                    //Canviar el div principal de la pàgina amb el rebut (la llista de enquestes)
                    $('.container').html(data.response);
                    omplirLlistaEnquestes();
                },
                error : function(xhr, textStatus, errorThrown) {
                   alert('Username o password no vàlids');
                }
            });
        }
    });
}

/*Validar els camps del formulari i aplicar les classes necessàries*/
function validaLogin(){
    //Guardem una variable si tots els camps són vàlids o no.
    var validat = true;
    $("input").each(function(){
        //En cas que estigui  buid afegir el missatge de validació
        if($(this).val().trim() == "") {
            $(this).parent().addClass('alert-validate');
            validat = false;
        }
        //Treure el missatge de validació quan l'usuari modifica l'input
        $(this).focus(function(){
            $(this).parent().removeClass('alert-validate');
        });
    });
    return validat;
}

/*Gestionarl l'estil dels botons del login (afegir i treure classes segons si estan buits o si tenen focus...) */
function gestionaEstilInputs(){
    /*Quan l'usuari surt del input comprovar si l'ha deixat buit o ple*/
    $("input").blur(function(){
        if($(this).val().trim() != "") {
            $(this).siblings('.label-input').addClass('has-val');
        }
        else {
            $(this).siblings('.label-input').removeClass('has-val');
        }
    });
    /*Comprovar si al carregar la pàgina algun camp esta ple*/
    setTimeout(function(){
        $("input").each(function(){
            if($(this).val() != "") {
                $(this).siblings('.label-input').addClass('has-val');
            }
        });
    }, 200);
}
