/*---------------------------------------------------------------------------------------*/
/*-------------------------------------[ ENQUESTES ]-------------------------------------*/

/*Quan es sotmet el formulari de una enquesta valida la resposta seleccionada i fa la petició ajax*/
function enquestaListener(){
    $(document).on('click', '.llista_submit', function(){
        var id = $(this).attr('id');
        id = id.substring(6, id.length);
        //Guardarem la resposta en una variable
        var answer;
        //Comprovem que només un dels dos estigui seleccionat
        if($('#si'+id).hasClass('active') && (!$('#no'+id).hasClass('active'))) answer = true;
        else if($('#no'+id).hasClass('active') && (!$('#si'+id).hasClass('active'))) answer= false;
        /*Si les dues estan seleccionades o cap està seleccionada no farà res*/
        let baseurl = window.location.origin+window.location.pathname;
        if(answer==true || answer==false){
            $.ajax({
                url: baseurl+'respon/'+id,
                type: 'POST',
                dataType: 'json',
                data: {
                    "answer": answer
                },
                async: true,
                success: function(resposta) {
                    //Rep l'html de l'enquesta complert i el substitueix
                    $('#enquesta_'+id).html(resposta.response);
                    //Assignar l'animació corresponent al pie que acabem de generar dinàmicament
                    fillPieId(id);
                }
            })
        }
    });
}

/*Fa una request de totes les enquestes, les coloca a la llista i activa les funcionalitats segons l'usuari que les demana*/
function omplirLlistaEnquestes(){
    let baseurl = window.location.origin+window.location.pathname;
    $.ajax({
        url: baseurl+'enquestes/totes',
        type: 'POST',
        dataType: 'json',
        async: true,
        success: function(resposta) {
            //Afegir totes les enquestes a la llista
            $('.llista_enquestes').html($('.llista_enquestes').html()+resposta.response);
            //Assignar animacions a tots els pies de la llista
            fillAllPies();
            //Amagar els stats de les enquestes
            amagaStats();
        },
        error : function(xhr, textStatus, errorThrown) {
           alert('Ajax request failed.');
        }
    });

}

/*Assignar les animacions a tots els pies de la pàgina*/
function fillAllPies(){
    $('.enquesta_presentacio, .enquesta_stats').each(function(){
        var si = parseInt($(this).find(".enquesta_si").text().substr(3, $(this).find(".enquesta_si").text().length));   //Guardar el numero de si
        var no = parseInt($(this).find(".enquesta_no").text().substr(3, $(this).find(".enquesta_no").text().length));   //Guardar el numero de no
        var perc = parseInt(100*si/(si+no));                                                                            //Calcular el % de si
        var pie = $(this).find(".pie");                                                                                 //Guardar el pie (pastís)
        if(!cssRuleExists('enquestes', 'fillPie'+perc))                                                                 //Si la animació existeix no cal crear-la
        {
            var enquestesCss = findStyleSheet('enquestes');                                                             //Guardar el fitxer css
            enquestesCss.insertRule("@keyframes fillPie"+perc+" { to { stroke-dasharray: "+perc+" 100; } }", 0);        //Crear l'animació al fitxer css
        }
        pie.find("circle").attr('style', 'animation: fillPie'+perc+' 1.5s ease-out forwards;');                         //Assignar (afegir) l'animació que hem creat (o no) al circle del pie corresponent
    });
}

/*Assignar la animació a un pie concret*/
function fillPieId(id){
    var enquesta = $('#enquesta_'+id).find('.enquesta_stats');
    var si = parseInt(enquesta.find(".enquesta_si").text().substr(3, enquesta.find(".enquesta_si").text().length));     //Guardar el numero de si
    var no = parseInt(enquesta.find(".enquesta_no").text().substr(3, enquesta.find(".enquesta_no").text().length));     //Guardar el numero de no
    var perc = parseInt(100*si/(si+no));                                                                                //Calcular el % de si
    var pie = enquesta.find(".pie");                                                                                    //Guardar el pie (pastís)
    if(!cssRuleExists('enquestes', 'fillPie'+perc))                                                                     //Si la animació existeix no cal crear-la
    {
        var enquestesCss = findStyleSheet('enquestes');                                                                 //Guardar el fitxer css
        enquestesCss.insertRule("@keyframes fillPie"+perc+" { to { stroke-dasharray: "+perc+" 100; } }", 0);            //Crear l'animació al fitxer css
    }
    pie.find("circle").attr('style', 'animation: fillPie'+perc+' 1.5s ease-out forwards;');                             //Assignar (afegir) l'animació que hem creat (o no) al circle del pie corresponent
}

/*Activar les funcionalitats de la llista (canvis de classe dels botons de resposta i mostrar/amagar stats)*/
function activaFuncionalitatLlista(){
    /*Toggle la classe actiu dels botons a la llista*/
    $(document).on('click', '.llista_button', function(){
        $(this).addClass("active");
        $(this).siblings().removeClass("active");
    });
    /*Toggle la visibilitat dels stats o del formulari de resposta*/
    $(document).on('click', '.enquesta_info', function(){
        $(this).siblings().slideToggle();
        $(this).parent().toggleClass('enquesta_llista_activa');
    });
}

/*Amagar les estadistiques*/
function amagaStats(){
    $('.enquesta_stats').each(function(){
        $(this).css('display', 'none');
    });
}

/*---------------------------------------------------------------------------------------*/
/*---------------------------------[ ANIMACIONS AL CSS ]---------------------------------*/

/*Busca un fitxer css concret (per nom) i el retorna*/
function findStyleSheet(nomCss) {
    var ss = document.styleSheets;                                                      //Guardar tots els css importats al document
    for (var i = 0; i < ss.length; ++i)
        if(ss[i].href.substr(ss[i].href.length - (nomCss.length+4))==(nomCss+".css"))   //Buscar el css concret: https://developer.mozilla.org/en-US/docs/Web/API/StyleSheet
            return ss[i];                                                               //Tornar el css
    return null;
}

/*Busca una animació per nom a un css concret (per nom) i la retorna*/
function cssRuleExists(nomCss, rule) {
    var ss = document.styleSheets;                                                      //Tots els css que estan link en el document
    for (var i = 0; i < ss.length; ++i)
        if(ss[i].href.substr(ss[i].href.length - (nomCss.length+4))==(nomCss+".css"))   //Buscar el css concret: https://developer.mozilla.org/en-US/docs/Web/API/StyleSheet
        {
            for (var j = 0; j < ss[i].cssRules.length; ++j)
            {
                if (ss[i].cssRules[j].type == window.CSSRule.KEYFRAMES_RULE && ss[i].cssRules[j].name == rule)  //Buscar la regla concreta: https://developer.mozilla.org/en-US/docs/Web/API/CSSRule
                {
                    return true;
                }
            }
            return false;
        }
    return false;
}
