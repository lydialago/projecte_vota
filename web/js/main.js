/*---------------------------------------------------------------------------------------*/
/*-------------------------------------[ PRINCIPAL ]-------------------------------------*/
$(document).ready(function(){
    /*login.js*/
    $('#username_input').focus();
    gestionaEstilInputs();
    loginListener();

    /*enquestes.js*/
    fillAllPies();
    enquestaListener();
    activaFuncionalitatLlista();
    if($(".container").find(".llista_enquestes").length) omplirLlistaEnquestes(); //En cas de recàrrega un cop loguejat
});
